var main =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp_name_"] = window["webpackJsonp_name_"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/js/index.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/components/cart.js":
/*!***********************************!*\
  !*** ./src/js/components/cart.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var animejs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! animejs */ \"./node_modules/animejs/anime.min.js\");\n/* harmony import */ var animejs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(animejs__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../util */ \"./src/js/util.js\");\n\n\n\nfunction deleteCartItem(item) {\n  animejs__WEBPACK_IMPORTED_MODULE_0___default()({\n    targets: item,\n    opacity: [1, 0],\n    height: [item.clientHeight, 0],\n    duration: 400,\n    easing: \"easeOutQuart\",\n    complete: function complete() {\n      item.remove();\n    }\n  });\n}\n\nfunction clearCart(markup) {\n  var container = document.querySelector(\".cart-container\");\n  animejs__WEBPACK_IMPORTED_MODULE_0___default()({\n    targets: container,\n    opacity: [1, 0],\n    duration: 400,\n    easing: \"easeOutQuart\",\n    complete: function complete() {\n      container.innerHTML = markup;\n      animejs__WEBPACK_IMPORTED_MODULE_0___default()({\n        targets: container,\n        opacity: [0, 1],\n        duration: 400,\n        easing: \"easeOutQuart\"\n      });\n    }\n  });\n}\n\nwindow.deleteCartItem = deleteCartItem;\nwindow.clearCart = clearCart;\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/cart.js?");

/***/ }),

/***/ "./src/js/components/content-text.js":
/*!*******************************************!*\
  !*** ./src/js/components/content-text.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function custom_resize() {\n  $('.content img').each(function (i, e) {\n    var w_post_img = $(e).width();\n    var h_post_img = w_post_img * 32 / 87;\n    $(e).css('height', h_post_img);\n  });\n  $('.gallery a').each(function (i, e) {\n    var w_gallery_img = $(e).width();\n    var h_gallery_img = w_gallery_img / 1.5;\n    $(e).css('height', h_gallery_img);\n  });\n  $('.gallery .item-thumbnail, .certificates .certificate-thumbnail').each(function (i, e) {\n    var w_gallery_img = $(e).width();\n    var h_gallery_img = w_gallery_img / 1.5;\n    $(e).css('height', h_gallery_img);\n  });\n}\n\ncustom_resize();\n$(window).resize(function () {\n  custom_resize();\n});\n/*     Обертка таблицы на текстовых    */\n\n$('.content-text > table').prev('h3').addClass('for_table');\n$(\".content-text > table\").wrap(\"<div class='table'><div class='table-responsive'></div></div>\");\n$('.content-text > .table').each(function () {\n  $(this).prev('h3.for_table').prependTo($(this));\n});\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/content-text.js?");

/***/ }),

/***/ "./src/js/components/cut-articles-text.js":
/*!************************************************!*\
  !*** ./src/js/components/cut-articles-text.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("$(\".article-card-info\").dotdotdot({});\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/cut-articles-text.js?");

/***/ }),

/***/ "./src/js/components/input-tel.js":
/*!****************************************!*\
  !*** ./src/js/components/input-tel.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("$('input[type=\"tel\"]').mask(\"+7 (999) 999-99-99\");\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/input-tel.js?");

/***/ }),

/***/ "./src/js/components/map.js":
/*!**********************************!*\
  !*** ./src/js/components/map.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var myMap;\nvar maps = [];\n/**\n * Иницилизация карты\n * Заранее требуется глобальная переменная myMap\n */\n\nfunction initYmap() {\n  $(\".map_container, .contacts-block__map-container\").each(function (i, e) {\n    if (!$(e).length) return false;\n    maps[i] = {\n      map_id: $(e).attr('id'),\n      markers: []\n    };\n    $(e).find('div').each(function (div_i, el) {\n      maps[i].markers.push({\n        lat: $(el).attr('data-lat').replace(/,/, '.'),\n        lng: $(el).attr('data-lng').replace(/,/, '.'),\n        popup: $(el).attr('data-popup'),\n        message: $(el).attr('data-message')\n      });\n      $(el).remove();\n    }); // Иницилизируем карту\n\n    myMap = new ymaps.Map(maps[i].map_id, {\n      center: [maps[i].markers[0].lat, maps[i].markers[0].lng],\n      zoom: 14,\n      controls: []\n    }, {\n      searchControlProvider: 'yandex#search'\n    }); // Создаём макет содержимого.\n\n    var MyIconContentLayout = ymaps.templateLayoutFactory.createClass('<div style=\"color: #FFFFFF; font-weight: bold;\">$[properties.iconContent]</div>'); // Добавляем на карту маркеры\n\n    var _iteratorNormalCompletion = true;\n    var _didIteratorError = false;\n    var _iteratorError = undefined;\n\n    try {\n      for (var _iterator = maps[i].markers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {\n        var marker = _step.value;\n        var placemark = new ymaps.Placemark([marker.lat, marker.lng], {\n          hintContent: marker.message,\n          balloonContent: marker.popup\n        });\n        myMap.geoObjects.add(placemark);\n      }\n    } catch (err) {\n      _didIteratorError = true;\n      _iteratorError = err;\n    } finally {\n      try {\n        if (!_iteratorNormalCompletion && _iterator.return != null) {\n          _iterator.return();\n        }\n      } finally {\n        if (_didIteratorError) {\n          throw _iteratorError;\n        }\n      }\n    }\n\n    myMap.events.add('sizechange', function () {\n      myMap.setBounds(myMap.geoObjects.getBounds(), {\n        checkZoomRange: true\n      });\n    });\n  });\n}\n\nif (window.ymaps !== undefined) ymaps.ready(function () {\n  initYmap();\n});\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/map.js?");

/***/ }),

/***/ "./src/js/components/mini-button.js":
/*!******************************************!*\
  !*** ./src/js/components/mini-button.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function miniButtonActive(button) {\n  button.classList.add(\"active\");\n}\n\nwindow.miniButtonActive = miniButtonActive;\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/mini-button.js?");

/***/ }),

/***/ "./src/js/components/product-card.js":
/*!*******************************************!*\
  !*** ./src/js/components/product-card.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function removeProduct(product) {\n  product.classList.add('removed');\n}\n\nfunction restoreProduct(product) {\n  product.classList.remove('removed');\n}\n\nfunction productCartAdded(product) {\n  var button = product.querySelector(\".product-card-add\");\n  button.disabled = true;\n  button.innerHTML = \"Добавлено\";\n}\n\nwindow.removeProduct = removeProduct;\nwindow.restoreProduct = restoreProduct;\nwindow.productCartAdded = productCartAdded;\ndocument.querySelectorAll(\".product-card-remove\").forEach(function (button) {\n  var product = button.closest(\".product-card\");\n  button.addEventListener(\"click\", function (event) {\n    removeProduct(product);\n  });\n});\ndocument.querySelectorAll(\".product-card-restore\").forEach(function (button) {\n  var product = button.closest(\".product-card\");\n  button.addEventListener(\"click\", function (event) {\n    restoreProduct(product);\n  });\n});\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/product-card.js?");

/***/ }),

/***/ "./src/js/components/shop-button.js":
/*!******************************************!*\
  !*** ./src/js/components/shop-button.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nvar ShopButton =\n/*#__PURE__*/\nfunction () {\n  function ShopButton(button) {\n    _classCallCheck(this, ShopButton);\n\n    this.button = button;\n    this.counter = this.button.querySelector(\".shop-button__count\");\n    this.initState();\n  }\n\n  _createClass(ShopButton, [{\n    key: \"changeCount\",\n    value: function changeCount(num) {\n      this.counter.innerHTML = parseInt(num);\n      this.initState();\n    }\n  }, {\n    key: \"initState\",\n    value: function initState() {\n      try {\n        if (this.counter.innerText === \"\" || parseInt(this.counter.innerText) <= 0) {\n          this.counter.classList.remove(\"filled\");\n        } else {\n          this.counter.classList.add(\"filled\");\n        }\n      } catch (_unused) {\n        console.log(\"У счетчика не число\", this.counter);\n      }\n    }\n  }]);\n\n  return ShopButton;\n}();\n\nvar cardButton = null;\nvar compareButton = null;\nvar favoritesButton = null;\n\nfunction initShopButtons() {\n  cardButton = new ShopButton(document.querySelector(\".cart-button\"));\n  compareButton = new ShopButton(document.querySelector(\".compare-button\"));\n  favoritesButton = new ShopButton(document.querySelector(\".favorites-button\"));\n}\n\ninitShopButtons(); /////////\n// API //\n/////////\n\nfunction changeCartCount(num) {\n  cardButton.changeCount(num);\n  return cardButton;\n}\n\nfunction changeCompareCount(num) {\n  compareButton.changeCount(num);\n  return compareButton;\n}\n\nfunction changeFavoritesCount(num) {\n  favoritesButton.changeCount(num);\n  return favoritesButton;\n} // Variables\n\n\nwindow.cardButton = cardButton;\nwindow.compareButton = compareButton;\nwindow.favoritesButton = favoritesButton; // Functions\n\nwindow.changeCartCount = changeCartCount;\nwindow.changeCompareCount = changeCompareCount;\nwindow.changeFavoritesCount = changeFavoritesCount;\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/components/shop-button.js?");

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _vendors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./vendors */ \"./src/js/vendors.js\");\n/* harmony import */ var _components_shop_button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/shop-button */ \"./src/js/components/shop-button.js\");\n/* harmony import */ var _components_shop_button__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components_shop_button__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _components_product_card__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/product-card */ \"./src/js/components/product-card.js\");\n/* harmony import */ var _components_product_card__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components_product_card__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _components_mini_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/mini-button */ \"./src/js/components/mini-button.js\");\n/* harmony import */ var _components_mini_button__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_mini_button__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _components_cut_articles_text__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/cut-articles-text */ \"./src/js/components/cut-articles-text.js\");\n/* harmony import */ var _components_cut_articles_text__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_cut_articles_text__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _components_map__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/map */ \"./src/js/components/map.js\");\n/* harmony import */ var _components_map__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_components_map__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var _components_content_text__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/content-text */ \"./src/js/components/content-text.js\");\n/* harmony import */ var _components_content_text__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_components_content_text__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var _components_input_tel__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/input-tel */ \"./src/js/components/input-tel.js\");\n/* harmony import */ var _components_input_tel__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_components_input_tel__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var _components_cart__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/cart */ \"./src/js/components/cart.js\");\n\n\n\n\n\n\n\n\n\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/index.js?");

/***/ }),

/***/ "./src/js/util.js":
/*!************************!*\
  !*** ./src/js/util.js ***!
  \************************/
/*! exports provided: parseHTML, parseArrayHTML, offset, svgRepairUse, createSVG, request, serialize */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"parseHTML\", function() { return parseHTML; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"parseArrayHTML\", function() { return parseArrayHTML; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"offset\", function() { return offset; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"svgRepairUse\", function() { return svgRepairUse; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"createSVG\", function() { return createSVG; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"request\", function() { return request; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"serialize\", function() { return serialize; });\n/**\n * Парсер HTML строки для ее перевода в HTML элементы\n * @param {string} markup HTML в виде строки\n * @returns {HTMLElement | Array<HTMLElement>} HTML элементы\n */\nfunction parseHTML(markup) {\n  var parser = new DOMParser();\n  var body = parser.parseFromString(markup, \"text/html\").body;\n\n  if (body.children.length > 1) {\n    var elements = new Array();\n    Array.prototype.slice.call(body.children).forEach(function (item) {\n      elements.push(item);\n    });\n    return elements;\n  } else {\n    return body.firstChild;\n  }\n}\n/**\n * Парсер массива HTML строк для перевода в массив HTML элементов\n * @param {Array<string>} markups Массив с html в виде строк\n */\n\nfunction parseArrayHTML(markups) {\n  var _this = this;\n\n  var elements = Array();\n  markups.forEach(function (markup) {\n    elements.push(_this.parseHTML(markup));\n  });\n  return elements;\n}\n/**\n * Получение отступов по документу\n * @param {HTMLElement} element\n */\n\nfunction offset(element) {\n  var rect = element.getBoundingClientRect(),\n      scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;\n  scrollTop = window.pageYOffset || document.documentElement.scrollTop;\n  return {\n    top: rect.top + scrollTop,\n    left: rect.left + scrollLeft\n  };\n}\n/**\n * Пересоздание тегов use в svg'шках\n * Помогает при выводе svg спрайтов ajax загрузки страницы\n */\n\nfunction svgRepairUse() {\n  var allSVG = Array.prototype.slice.call(document.querySelectorAll('svg'));\n  allSVG.forEach(function (svg) {\n    if (svg.firstElementChild.href !== undefined) {\n      var href = svg.firstElementChild.href.baseVal;\n      var use = document.createElementNS('http://www.w3.org/2000/svg', 'use');\n      use.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', href);\n      svg.firstElementChild.remove();\n      svg.appendChild(use);\n    }\n  });\n}\n/**\n * Создание svg элемента в документе\n * @param {string} href ссылка на svg\n * @param {string} className класс для svg элемента\n * @returns {SVGElement} svg элемент\n */\n\nfunction createSVG(href) {\n  var className = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';\n  var svg = document.createElementNS(\"http://www.w3.org/2000/svg\", \"svg\");\n  var use = document.createElementNS('http://www.w3.org/2000/svg', 'use');\n  use.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', href);\n  svg.classList.add(className);\n  svg.appendChild(use);\n  return svg;\n}\nfunction request(data, action, method) {\n  var success = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function (response) {};\n  var error = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : function (error) {};\n  var url = new URL(action);\n  method = method.toLowerCase();\n  if (method == 'get') url.search = data;\n  return new Promise(function (resolve, reject) {\n    var xhr = new XMLHttpRequest();\n    xhr.open(method, url.href, true);\n\n    if (method === 'post') {\n      if (typeof data === 'string') {\n        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');\n      }\n    }\n\n    xhr.setRequestHeader(\"X-Requested-With\", \"XMLHttpRequest\");\n\n    xhr.onload = function () {\n      if (this.status == 200) {\n        resolve(JSON.parse(this.response));\n      } else {\n        var err = new Error(this.statusText);\n        err.code = this.status;\n        reject(err);\n      }\n    };\n\n    xhr.send(data);\n  }).then(success, error);\n}\nfunction serialize(form) {\n  var formData = new FormData(form);\n  var arrayData = new Array();\n  var data = new String();\n  var _iteratorNormalCompletion = true;\n  var _didIteratorError = false;\n  var _iteratorError = undefined;\n\n  try {\n    for (var _iterator = formData.entries()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {\n      var item = _step.value;\n      arrayData.push(item);\n    }\n  } catch (err) {\n    _didIteratorError = true;\n    _iteratorError = err;\n  } finally {\n    try {\n      if (!_iteratorNormalCompletion && _iterator.return != null) {\n        _iterator.return();\n      }\n    } finally {\n      if (_didIteratorError) {\n        throw _iteratorError;\n      }\n    }\n  }\n\n  arrayData.forEach(function (item, index) {\n    if (index) data += '&';\n    data += item[0] + '=' + encodeURIComponent(item[1]);\n  });\n  return data;\n}\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/util.js?");

/***/ }),

/***/ "./src/js/vendors.js":
/*!***************************!*\
  !*** ./src/js/vendors.js ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ \"./node_modules/jquery/dist/jquery.js\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bootstrap */ \"./node_modules/bootstrap/dist/js/bootstrap.js\");\n/* harmony import */ var bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bootstrap__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var dotdotdot__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! dotdotdot */ \"./node_modules/dotdotdot/src/js/jquery.dotdotdot.js\");\n/* harmony import */ var dotdotdot__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(dotdotdot__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\n\n//# sourceURL=webpack://%5Bname%5D/./src/js/vendors.js?");

/***/ })

/******/ });