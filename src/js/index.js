import "./vendors"

import "./components/shop-button"
import "./components/product-card"
import "./components/mini-button"

import "./components/cut-articles-text";

import "./components/map";
import "./components/content-text";
import "./components/input-tel";
import "./components/cart";
