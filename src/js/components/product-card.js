function removeProduct(product) {
    product.classList.add('removed')
}

function restoreProduct(product) {
    product.classList.remove('removed')
}

function productCartAdded(product) {
    var button = product.querySelector(".product-card-add")
    button.disabled = true
    button.innerHTML = "Добавлено"
}

window.removeProduct = removeProduct
window.restoreProduct = restoreProduct
window.productCartAdded = productCartAdded

document.querySelectorAll(".product-card-remove").forEach(function(button) {
    var product = button.closest(".product-card")
    button.addEventListener("click", function(event) {removeProduct(product)})
})

document.querySelectorAll(".product-card-restore").forEach(function(button) {
    var product = button.closest(".product-card")
    button.addEventListener("click", function(event) {restoreProduct(product)})
})
