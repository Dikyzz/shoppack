var myMap;
var maps = [];


/**
 * Иницилизация карты
 * Заранее требуется глобальная переменная myMap
 */

function initYmap() {
    $(".map_container, .contacts-block__map-container").each(function (i, e) {
        if (!$(e).length) return false;

        maps[i] = {
            map_id: $(e).attr('id'),
            markers: []
        };
        $(e).find('div').each(function (div_i, el) {
            maps[i].markers.push({
                lat: $(el).attr('data-lat').replace(/,/, '.'),
                lng: $(el).attr('data-lng').replace(/,/, '.'),
                popup: $(el).attr('data-popup'),
                message: $(el).attr('data-message')
            });
            $(el).remove();
        });

        // Иницилизируем карту
        myMap = new ymaps.Map(maps[i].map_id, {
            center: [maps[i].markers[0].lat, maps[i].markers[0].lng],
            zoom: 14,
            controls: [],
        }, {
            searchControlProvider: 'yandex#search'
        });

        // Создаём макет содержимого.
        var MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
            '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
        );


        // Добавляем на карту маркеры
        for (var marker of maps[i].markers) {
            var placemark = new ymaps.Placemark([marker.lat, marker.lng], {
                hintContent: marker.message,
                balloonContent: marker.popup,
            });

            myMap.geoObjects.add(placemark);
        }
        myMap.events.add('sizechange', function () {
            myMap.setBounds(myMap.geoObjects.getBounds(), {
                checkZoomRange: true
            });
        });
    });

}

if (window.ymaps !== undefined)
    ymaps.ready(function () {
        initYmap()
    })